package net.havenrealms.havenvehicles.objects;

import net.havenrealms.havenitems.objects.CustomItem;
import net.havenrealms.havenvehicles.HavenVehicles;
import net.havenrealms.havenvehicles.objects.classes.UUIDDataType;
import net.minecraft.server.v1_16_R3.EntityArmorStand;
import net.minecraft.server.v1_16_R3.World;
import org.bukkit.Location;
import org.bukkit.NamespacedKey;
import org.bukkit.craftbukkit.v1_16_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_16_R3.inventory.CraftItemStack;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import javax.xml.stream.events.Namespace;

public class CustomVehicleEntity extends EntityArmorStand {

    public ArmorStand entity;
    public CustomVehicle vehicle;

    public CustomVehicleEntity(HavenVehicles plugin, Entity vehicleEntity, World world, double lx, double ly, double lz) {

        // Parent for NMS
        super(world, lx, ly, lz);

        // Setup Bukkit Entity
        this.entity = (ArmorStand) vehicleEntity;

        // Setup Spawn Item
        ItemStack itemStack = this.entity.getEquipment().getHelmet();

        // Load Custom Vehicle Class
        this.vehicle = plugin.vehicleManager.getVehicle(itemStack);
    }

    public CustomVehicleEntity(CustomVehicle vehicleClass, NamespacedKey entityKey, Player owner, World world, double lx, double ly, double lz, float lyaw, float lpitch, ItemStack spawnItem) {

        // Parent for NMS
        super(world, lx, ly, lz);

        // Use Player Direction
        this.setYawPitch(lyaw, lpitch);

        // Setup Bukkit Entity
        this.entity = (ArmorStand) this.getBukkitEntity();
        this.vehicle = vehicleClass;

        // Setup Variable Data
        PersistentDataContainer entityContainer = this.entity.getPersistentDataContainer();
        entityContainer.set(entityKey, new UUIDDataType(), owner.getUniqueId());

        // Make Entity Invisible
        this.entity.setBasePlate(false);
        this.entity.setVisible(true);

        // Silence Vehicle Entity (Vanilla)
        this.entity.setSilent(true);

        // Setup Vehicle Item
        this.entity.getEquipment().setHelmet(spawnItem);
    }

    public Entity getEntity() {
        return this.entity;
    }
}
