package net.havenrealms.havenvehicles.objects;

import com.comphenix.protocol.events.PacketEvent;
import net.havenrealms.havenvehicles.HavenVehicles;
import net.havenrealms.havenvehicles.objects.classes.UUIDDataType;
import net.minecraft.server.v1_16_R3.EntityArmorStand;
import net.minecraft.server.v1_16_R3.EntityTypes;
import net.minecraft.server.v1_16_R3.PacketPlayInSteerVehicle;
import org.bukkit.*;
import org.bukkit.craftbukkit.v1_16_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_16_R3.entity.CraftArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataContainer;

import java.util.UUID;

public class VehicleListener implements Listener {

    public HavenVehicles plugin;

    public VehicleListener(HavenVehicles instance) {
        this.plugin = instance;
    }

    @EventHandler
    public void VehicleClickEvent(PlayerInteractAtEntityEvent event) {
        Player player = event.getPlayer();
        Entity clickEntity = event.getRightClicked();
        if(clickEntity.getType() == EntityType.ARMOR_STAND) {
            PersistentDataContainer entityDataContainer = clickEntity.getPersistentDataContainer();
            if(entityDataContainer.has(this.plugin.vehicleKey, new UUIDDataType())) {

                // Check Owner
                UUID ownerID = entityDataContainer.get(this.plugin.vehicleKey, new UUIDDataType());
                if (ownerID.equals(player.getUniqueId())) {

                    // Get Entity Class
                    CraftArmorStand vehicleCraft = ((CraftArmorStand)clickEntity);
                    EntityArmorStand vehicleEntity = vehicleCraft.getHandle();
                    Location spawnLocation = vehicleEntity.getBukkitEntity().getLocation();
                    org.bukkit.World spawnWorld = spawnLocation.getWorld();
                    Double spawnX = spawnLocation.getX();
                    Double spawnY = spawnLocation.getY();
                    Double spawnZ = spawnLocation.getZ();
                    net.minecraft.server.v1_16_R3.World craftWorld = ((CraftWorld) spawnWorld).getHandle();
                    CustomVehicleEntity vehicleClass = new CustomVehicleEntity(this.plugin.instance, vehicleEntity.getBukkitEntity(), craftWorld, spawnX, spawnY, spawnZ);
                    CustomVehicle vehicleMeta = vehicleClass.vehicle;

                    // Reset Vehicle Speed
                    vehicleMeta.resetSpeed();

                    // Mount Player
                    clickEntity.addPassenger(player);

                    // Setup Player Entity Mapping
                    PersistentDataContainer playerContainer = player.getPersistentDataContainer();
                    playerContainer.set(this.plugin.vehicleKey, new UUIDDataType(), clickEntity.getUniqueId());
                } else {
                    player.sendMessage("This is not your vehicle.");
                }

                // Cancel Event to Stop Removing Item
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void VehiclePlaceEvent(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if(event.getHand().equals(EquipmentSlot.HAND)) {
            if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                ItemStack clickItem = player.getInventory().getItemInMainHand();
                boolean validItem = true;
                if(clickItem.getType() == Material.AIR) {
                    validItem = false;
                }
                if(clickItem.equals(null)) {
                    validItem = false;
                }
                if(validItem) {
                    if (this.plugin.vehicleManager.isVehicle(clickItem)) {

                        // Get Active Vehicle Class
                        CustomVehicle vehicleClass = this.plugin.vehicleManager.getVehicle(clickItem);

                        // Cancel Event for Player
                        event.setCancelled(true);

                        // Define Variables
                        Location spawnLocation = event.getClickedBlock().getRelative(event.getBlockFace()).getLocation();
                        Float spawnYaw = player.getLocation().getYaw();
                        Float spawnPitch = spawnLocation.getPitch();

                        // Debug
                        player.sendMessage(String.valueOf(spawnLocation.getYaw()));
                        player.sendMessage(player.getFacing().toString());

                        // Center Spawn Position
                        spawnLocation.add(0.5, 0, 0.5);

                        // Setup Spawn Location
                        World spawnWorld = spawnLocation.getWorld();
                        Double spawnX = spawnLocation.getX();
                        Double spawnY = spawnLocation.getY();
                        Double spawnZ = spawnLocation.getZ();
                        net.minecraft.server.v1_16_R3.World craftWorld = ((CraftWorld) spawnWorld).getHandle();

                        // Get Spawn Item
                        CustomVehicleEntity vehicleEntity = new CustomVehicleEntity(vehicleClass, this.plugin.vehicleKey, player, craftWorld, spawnX, spawnY, spawnZ, spawnYaw, spawnPitch, clickItem);
                        craftWorld.addEntity(vehicleEntity, CreatureSpawnEvent.SpawnReason.CUSTOM);

                        // Remove Spawn Item if Needed
                        if (!player.getGameMode().equals(GameMode.CREATIVE)) {
                            player.getInventory().setItemInMainHand(null);
                        }
                    } else {
                        player.sendMessage("This is not a vehicle.");
                    }
                }
            }
        }
    }

    public Entity getEntityByUUID(UUID uuid) {
        for(World world : Bukkit.getWorlds()) {
            for(Chunk chunk : world.getLoadedChunks()) {
                for(Entity entity : chunk.getEntities()) {
                    if(entity.getUniqueId().equals(uuid)) {
                        return entity;
                    }
                }
            }
        }
        return null;
    }
}
