package net.havenrealms.havenvehicles.objects;

import net.havenrealms.havenitems.objects.CustomItem;
import org.bukkit.configuration.ConfigurationSection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomVehicle {

    private VehicleManager manager;
    public ConfigurationSection config;

    public String vehicleName;
    public String vehicleModel;
    public String vehiclePower;
    public String vehicleType;
    public Boolean vehicleSmall;
    public List<String> vehicleVariations;
    public Map<String, CustomItem> vehicleItems;

    public float speedValue;
    public float speedRange;
    public float speedChange;

    public CustomVehicle(VehicleManager vehicleManager, ConfigurationSection section) {

        // Define Variables
        this.manager = vehicleManager;
        this.config = section;
        this.vehicleName = this.config.getName();
        this.vehicleItems = new HashMap<String, CustomItem>();

        // Load Vehicle Physics Type
        if(!this.config.contains("type")) {
            this.manager.plugin.logger.warning("Failed to load " + this.vehicleName + ": Vehicle does not have a valid type.");
            return;
        } else {
            this.vehicleType = this.config.getString("type");
            if(!VehicleTypes.contains(this.vehicleType)) {
                this.manager.plugin.logger.warning("Failed to load " + this.vehicleName + ": Vehicle type \"" + this.vehicleType + "\" is not a valid vehicle type");
                return;
            }
        }

        // Load Vehicle Power Type
        if(!this.config.contains("power")) {
            this.manager.plugin.logger.warning("Failed to load " + this.vehicleName + ": Vehicle does not have a valid power source.");
            return;
        } else {
            this.vehiclePower = this.config.getString("power");
            if(!VehiclePowerTypes.contains(this.vehiclePower)) {
                this.manager.plugin.logger.warning("Failed to load " + this.vehicleName + ": Vehicle power type \"" + this.vehiclePower + "\" is not a valid vehicle power source.");
                return;
            }
        }

        // Load Vehicle isSmall
        if(!this.config.contains("small")) {
            this.vehicleSmall = false;
        } else {
            this.vehicleSmall = this.config.getBoolean("small");
        }

        // Load Vehicle Model String
        if(!this.config.contains("model")) {
            this.manager.plugin.logger.warning(this.vehicleName + " does not have a valid model string, using name instead. Please fix this in the configuration.");
            this.vehicleModel = this.vehicleName;
        } else {
            this.vehicleModel = this.config.getString("model");
        }

        // Load Vehicle Variations
        if(this.config.contains("variations")) {
            this.vehicleVariations = new ArrayList<String>();
            List<String> variations = this.config.getStringList("variations");
            for(String variation: variations) {
                String newName = variation + this.vehicleName;
                if(this.manager.plugin.itemManager.itemManager.hasItem(newName)) {
                    CustomItem vehicleItem = this.manager.plugin.itemManager.itemManager.getItem(newName);
                    this.vehicleVariations.add(newName);
                    this.vehicleItems.put(newName, vehicleItem);
                } else {
                    this.manager.plugin.logger.warning(newName + " is not a valid item and will not be added.");
                }
            }
        } else {
            if(this.manager.plugin.itemManager.itemManager.hasItem(this.vehicleName)) {
                CustomItem vehicleItem = this.manager.plugin.itemManager.itemManager.getItem(this.vehicleName);
                this.vehicleItems.put(this.vehicleName, vehicleItem);
            } else {
                this.manager.plugin.logger.warning(this.vehicleName + " is not a valid item and will not be added.");
                return;
            }
        }

        // Load Physics Variables
        if(!this.config.contains("speed")) {
            this.manager.plugin.logger.warning(this.vehicleName + " does not contain any speed variables, this means it won't move when driven.");
        } else {
            ConfigurationSection speedSection = this.config.getConfigurationSection("speed");

            // Check Speed Value
            if(!speedSection.contains("value")) {
                this.manager.plugin.logger.warning(this.vehicleName + " does not have a speed value set.");
                return;
            } else {
                this.speedValue = speedSection.getInt("value");
            }

            // Check Speed Range
            if(!speedSection.contains("range")) {
                this.manager.plugin.logger.warning(this.vehicleName + " does not have a speed range. Slow down and take off will have no affect on this vehicle.");
                this.speedRange = 1;
            } else {
                this.speedRange = speedSection.getInt("range");
            }
        }

        this.manager.plugin.logger.info("Loaded Vehicle: " + this.vehicleName);
        if(this.vehicleVariations != null) {
            if(!this.vehicleVariations.isEmpty()) {
                this.manager.plugin.logger.info("Variations: " + this.vehicleVariations.toString());
            }
        }
    }

    public void speedUp() {
        if(this.speedValue < this.speedRange) {
            this.speedValue = this.speedValue + 1;
        }
        this.changeSpeed();
    }

    public void slowDown() {
        if(this.speedValue > 0) {
            this.speedValue = this.speedValue - 1;
        }
        this.changeSpeed();
    }

    private void changeSpeed() {
        this.speedChange = this.speedRange/this.speedValue;
    }

    public void resetSpeed() {
        this.speedValue = 0;
    }

}
