package net.havenrealms.havenvehicles.objects;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import net.minecraft.server.v1_16_R3.EntityArmorStand;
import net.minecraft.server.v1_16_R3.PacketPlayInSteerVehicle;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_16_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_16_R3.entity.CraftArmorStand;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;

import java.lang.reflect.Method;
import java.util.function.Supplier;

public class SteeringListener extends PacketAdapter {

    private VehicleManager manager;
    private Method[] methods;

    public SteeringListener(Plugin plugin, VehicleManager vehicleManager, ListenerPriority listenerPriority, PacketType[] types) {
        super(plugin, listenerPriority, types);
        this.manager = vehicleManager;
    }

    @Override
    public void onPacketReceiving(PacketEvent event) {
        if(event.getPacketType().equals(PacketType.Play.Client.STEER_VEHICLE)){
            PacketPlayInSteerVehicle steerPacket = (PacketPlayInSteerVehicle) event.getPacket().getHandle();


            // Read Pack Data
            boolean space = steerPacket.d();
            boolean shift = steerPacket.e();
            float side = steerPacket.b();
            float forward = steerPacket.c();

            // Define Variables
            Player player = event.getPlayer();
            Entity entity = player.getVehicle();

            // Get Entity Class
            CraftArmorStand vehicleCraft = ((CraftArmorStand)entity);
            EntityArmorStand vehicleEntity = vehicleCraft.getHandle();
            Location spawnLocation = vehicleEntity.getBukkitEntity().getLocation();
            org.bukkit.World spawnWorld = spawnLocation.getWorld();
            Double spawnX = spawnLocation.getX();
            Double spawnY = spawnLocation.getY();
            Double spawnZ = spawnLocation.getZ();
            net.minecraft.server.v1_16_R3.World craftWorld = ((CraftWorld) spawnWorld).getHandle();
            CustomVehicleEntity vehicleClass = new CustomVehicleEntity(this.manager.plugin.instance ,vehicleEntity.getBukkitEntity(), craftWorld, spawnX, spawnY, spawnZ);

            // Get Vehicle Meta
            CustomVehicle vehicleMeta = vehicleClass.vehicle;

            // Debug
            player.sendMessage("Forward: " + String.valueOf(forward) + " | Side: " + String.valueOf(side) + " | Shift: " + shift + " | " + " Space: " + space);
            player.sendMessage("Speed Value: " + vehicleMeta.speedValue + " | Speed Range: " + vehicleMeta.speedRange);

            // Player Exiting Vehicle
            if(shift) {
                this.manager.PlayerExitVehicle(player);
            }

            // Check if Driving Forward
            if(forward > 0) {
                vehicleClass.vehicle.speedUp();
            } else if (forward < 0) {
                vehicleClass.vehicle.speedUp();
            } else {
                vehicleClass.vehicle.slowDown();
            }

            // Check if Can Drive
            boolean canDrive = false;
            if(vehicleClass.vehicle.speedValue > 0) {
                canDrive = true;
            }

            if(canDrive) {
                // Vehicle Physics
                switch (vehicleMeta.vehicleType) {
                    case "WATER":
                        break;
                    case "LAND":
                        Vector vehicleVector = vehicleClass.entity.getLocation().getDirection();
                        break;
                    case "PLANE":
                        break;
                }

                // Vehicle Location Method
                this.methods = ((Supplier<Method[]>) () -> {
                    try {
                        Method getHandle = Class.forName(Bukkit.getServer().getClass().getPackage().getName() + ".entity.CraftEntity").getDeclaredMethod("getHandle");
                        return new Method[] {
                                getHandle, getHandle.getReturnType().getDeclaredMethod("setPositionRotation", double.class, double.class, double.class, float.class, float.class)
                        };
                    } catch (Exception ex) {
                        return null;
                    }
                }).get();
                switch (vehicleMeta.vehicleType) {
                    case "LAND":
                        try {
                            float vehicleSpeed = vehicleClass.vehicle.speedChange * vehicleClass.vehicle.speedValue;
                            player.sendMessage("Speed Change: " + String.valueOf(vehicleMeta.speedChange) + " | Speed Value: " + String.valueOf(vehicleMeta.speedValue) + " | Vehicle Speed: " + String.valueOf(vehicleSpeed));
                            Vector vehicleVector = vehicleClass.entity.getLocation().getDirection().multiply(vehicleClass.vehicle.speedChange);
                            Location currentLocation = vehicleClass.entity.getLocation();
                            Location teleport = vehicleVector.toLocation(vehicleClass.entity.getWorld());
                            Location newLocation = teleport.add(currentLocation);
                            player.sendMessage("Vehicle Vector: " + vehicleVector.toString());
                            player.sendMessage("New Location: " + newLocation.toString());
                            player.sendMessage("Cur Location: " + vehicleClass.entity.getLocation().toString());
                            Location loc = newLocation;
                            float playerYaw = player.getEyeLocation().getYaw();
                            this.methods[1].invoke(this.methods[0].invoke(entity), loc.getX(), loc.getY(), loc.getZ(), playerYaw, loc.getPitch());
                        } catch (Exception ex) {
                            // Shouldn't happen, but possibly print some stuff?
                        }
                        player.sendMessage("VYaw: " + String.valueOf(vehicleClass.entity.getLocation().getYaw()));
                        break;
                }
            }
        }
    }

}
