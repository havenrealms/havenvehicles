package net.havenrealms.havenvehicles.objects;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

public class VehicleConfiguration {

    protected final boolean createIfNotExist, resource;
    protected final JavaPlugin plugin;

    protected YamlConfiguration config;
    protected File file, path;
    protected String name;

    private VehicleConfiguration(JavaPlugin instance, File path, String name, boolean createIfNotExist, boolean resource) throws IOException, InvalidConfigurationException {
        this.plugin = instance;
        this.path = path;
        this.name = name;
        this.createIfNotExist = createIfNotExist;
        this.resource = resource;
        create();
    }

    public VehicleConfiguration(JavaPlugin instance, String path, String name, boolean createIfNotExist, boolean resource) throws IOException, InvalidConfigurationException {
        this(instance, new File(path), name, createIfNotExist, resource);
    }

    public YamlConfiguration getConfig() throws IOException, InvalidConfigurationException {
        if(this.config == null) {
            return reloadConfig();
        }
        return this.config;
    }

    public void create() throws IOException, InvalidConfigurationException {
        if(file == null) {
            reloadFile();
        }
        if(this.config == null) {
            reloadConfig();
        }
        if(!this.createIfNotExist || this.file.exists()) {
            return;
        }

        file.getParentFile().mkdirs();
        if(this.resource) {
            this.plugin.saveResource(name, false);
        } else {
            try {
                this.file.createNewFile();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void reload() throws IOException, InvalidConfigurationException {
        this.reloadFile();
        this.reloadConfig();
    }
    public File reloadFile() {
        this.file = new File(this.path, this.name);
        return this.file;
    }

    public YamlConfiguration reloadConfig() throws IOException, InvalidConfigurationException {
        if(this.file == null) {
            reloadFile();
        }
        try {
            this.config = new YamlConfiguration();
            this.config.load(this.file);
            return this.config;
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}