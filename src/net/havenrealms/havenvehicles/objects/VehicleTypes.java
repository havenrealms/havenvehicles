package net.havenrealms.havenvehicles.objects;

import java.awt.*;

public enum VehicleTypes {
    WATER,
    LAND,
    UNDERWATER,
    PLANE,
    HELICOPTER;

    public static boolean contains(String value) {
        for (VehicleTypes vehicleType : VehicleTypes.values()) {
            if (vehicleType.name().equalsIgnoreCase(value)) {
                return true;
            }
        }
        return false;
    }
}