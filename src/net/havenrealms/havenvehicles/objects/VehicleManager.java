package net.havenrealms.havenvehicles.objects;

import net.havenrealms.havenitems.objects.CustomItem;
import net.havenrealms.havenvehicles.HavenVehicles;
import net.havenrealms.havenvehicles.objects.classes.UUIDDataType;
import org.bukkit.NamespacedKey;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.*;

public class VehicleManager {

    private Map<String, CustomVehicle> customVehicles;
    public HavenVehicles plugin;
    private File vehicleDirectory;
    private File[] vehicleFiles;
    private List<VehicleConfiguration> vehicleConfs;

    public VehicleManager(HavenVehicles plugin) throws IOException, InvalidConfigurationException {

        // Define Variables
        this.plugin = plugin;
        this.vehicleDirectory = this.plugin.vehicleDirectory;

        // Load All Vehicle Files
        this.vehicleFiles = this.vehicleDirectory.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.endsWith(".yml");
            }
        });


        reloadVehicles();
    }

    public boolean isVehicle(ItemStack item) {
        NamespacedKey itemKey = new NamespacedKey(this.plugin.itemManager.instance, "havenvehicles");
        ItemMeta itemMeta = item.getItemMeta();
        PersistentDataContainer dataContainer = itemMeta.getPersistentDataContainer();
        if(dataContainer.has(itemKey, PersistentDataType.STRING)) {
            String vehicleID = dataContainer.get(itemKey, PersistentDataType.STRING);
            if(vehicleID != null) {
                return true;
            }
        }
        return false;
    }

    public CustomVehicle getVehicle(ItemStack item) {
        NamespacedKey itemKey = new NamespacedKey(this.plugin.itemManager.instance, "havenvehicles");
        ItemMeta itemMeta = item.getItemMeta();
        PersistentDataContainer dataContainer = itemMeta.getPersistentDataContainer();
        if(dataContainer.has(itemKey, PersistentDataType.STRING)) {
            String vehicleID = dataContainer.get(itemKey, PersistentDataType.STRING);
            if(vehicleID != null) {
                CustomVehicle vehicle = this.customVehicles.get(vehicleID);
                return vehicle;
            }
        }
        return null;
    }

    public void PlayerExitVehicle(Player player) {
        PersistentDataContainer playerContainer = player.getPersistentDataContainer();
        if(playerContainer.has(this.plugin.vehicleKey, new UUIDDataType())) {
            playerContainer.remove(this.plugin.vehicleKey);
        }
    }

    public void reloadVehicles() throws IOException, InvalidConfigurationException {

        // Reset Custom Vehicles Map
        this.plugin.logger.info("Loading vehicles...");
        this.customVehicles = new HashMap<String, CustomVehicle>();

        // Setup Item File Configurations
        for (File vehicleConf : this.vehicleFiles) {

            VehicleConfiguration newItemConf = new VehicleConfiguration(plugin, vehicleConf.getParent(), vehicleConf.getName(), false, false);

            // Use Subsections
            YamlConfiguration vehicleConfig = newItemConf.getConfig();
            ConfigurationSection vehicleConfigSection = vehicleConfig.getConfigurationSection("vehicles");
            if(vehicleConfigSection == null) {
                this.plugin.logger.warning("Failed to load vehicles section from " + newItemConf.name);
                continue;
            }
            Set<String> vehicles = new HashSet<String>();
            try {
                for(String vehicle : vehicleConfigSection.getKeys(false)) {
                    vehicles.add(vehicle);
                }
            } catch (NullPointerException e) {
                this.plugin.logger.warning("Failed to load vehicles from " + newItemConf.name);
                e.printStackTrace();
            }

            if(vehicles != null) {
                this.plugin.logger.info(newItemConf.name + " was successfully loaded.");
                for(String vehicle : vehicles) {
                    ConfigurationSection vehicleSection = vehicleConfig.getConfigurationSection("vehicles." + vehicle);
                    CustomVehicle customVehicle = new CustomVehicle(this, vehicleSection);
                    if(customVehicle.vehicleVariations == null) {
                        customVehicles.put(customVehicle.vehicleName, customVehicle);
                    } else {
                        if(!customVehicle.vehicleVariations.isEmpty()) {
                            for(String variation : customVehicle.vehicleVariations) {
                                customVehicles.put(variation, customVehicle);
                            }
                        } else {
                            customVehicles.put(customVehicle.vehicleName, customVehicle);
                        }
                    }
                }
            } else {
                this.plugin.logger.warning("There were no vehicles found in " + newItemConf.name);
            }
        }
    }
}
