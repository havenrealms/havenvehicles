package net.havenrealms.havenvehicles.objects;

import java.util.HashSet;

public enum VehiclePowerTypes {
    PETROL,
    ELECTRIC,
    MAGIC;

    public static boolean contains(String value) {
        HashSet<String> values = new HashSet<>();
        for(VehiclePowerTypes type : VehiclePowerTypes.values()) {
            values.add(type.name());
        }

        return values.contains(value);
    }
}
