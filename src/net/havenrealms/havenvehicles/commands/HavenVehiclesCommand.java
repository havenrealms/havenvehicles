package net.havenrealms.havenvehicles.commands;

import net.havenrealms.havenvehicles.HavenVehicles;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class HavenVehiclesCommand implements CommandExecutor {

    private HavenVehicles plugin;
    private List<String> commandArgs;

    public HavenVehiclesCommand(HavenVehicles instance) {
        this.plugin = instance;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        // Load Command Arguments
        this.commandArgs = Arrays.asList(args);

        // Check Arguments Empty
        if(commandArgs.isEmpty()) {
            sender.sendMessage("Type \"/" + label +" help\" for more help.");
            return true;
        }

        // Process Arguments
        String defaultArg = commandArgs.get(0);
        switch(defaultArg) {
            case "reload":
                try {
                    if(sender instanceof Player) {
                        sender.sendMessage("Reloading Vehicles...");
                    }
                    this.plugin.logger.info("Reloading Vehicles...");
                    this.plugin.vehicleManager.reloadVehicles();
                    if(sender instanceof Player) {
                        sender.sendMessage("All vehicles were successfully loaded.");
                    }
                    this.plugin.logger.info("All vehicles were successfully loaded.");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InvalidConfigurationException e) {
                    e.printStackTrace();
                }
                break;
        }
        return true;
    }
}
