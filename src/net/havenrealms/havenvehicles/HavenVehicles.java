package net.havenrealms.havenvehicles;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.ListenerPriority;
import net.havenrealms.havenitems.HavenItems;
import net.havenrealms.havenvehicles.commands.HavenVehiclesCommand;
import net.havenrealms.havenvehicles.objects.SteeringListener;
import net.havenrealms.havenvehicles.objects.VehicleListener;
import net.havenrealms.havenvehicles.objects.VehicleManager;
import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.plugin.java.JavaPlugin;

import javax.naming.Name;
import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

public class HavenVehicles extends JavaPlugin {

    public HavenVehicles instance;
    public HavenItems itemManager;
    public Logger logger;
    public File pluginDirectory;
    public File vehicleDirectory;
    public VehicleManager vehicleManager;
    public NamespacedKey vehicleKey;
    public ProtocolManager manager;

    public HavenVehicles() {
        if(this.instance != null) {
            throw new Error("Haven Vehicles has already been initialized.");
        }
        this.instance = this;
    }

    public HavenVehicles getInstance() {
        return this.instance;
    }

    @Override
    public void onEnable() {

        // Register Listeners
        this.getServer().getPluginManager().registerEvents(new VehicleListener(this.instance), this);

        // Setup Logger
        this.logger = this.getLogger();

        // Setup Data Key
        this.vehicleKey = new NamespacedKey(this, "havenitems");

        // Setup Directory Structure
        this.pluginDirectory = new File(this.getDataFolder() + File.separator);
        if(!this.pluginDirectory.exists()) {
            this.pluginDirectory.mkdir();
        }
        this.vehicleDirectory = new File(this.getDataFolder() + File.separator + "vehicles" + File.separator);
        if(!this.vehicleDirectory.exists()) {
            this.vehicleDirectory.mkdir();
        }

        // Setup Haven Items Instance
        if(Bukkit.getServer().getPluginManager().isPluginEnabled("HavenItems")) {
            this.itemManager = (HavenItems)Bukkit.getServer().getPluginManager().getPlugin("HavenItems");

        } else {
            throw new Error("Haven Items is a dependency of Haven Vehicles and must be installed.");
        }

        // Setup Vehicles Manager
        try {
            this.vehicleManager = new VehicleManager(this.instance);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        }

        // Setup ProtocolLib
        this.manager = ProtocolLibrary.getProtocolManager();
        this.manager.addPacketListener(new SteeringListener(this, this.vehicleManager,  ListenerPriority.HIGHEST, new PacketType[]{PacketType.Play.Client.STEER_VEHICLE}));

        // Register Commands
        this.getCommand("havenvehicles").setExecutor(new HavenVehiclesCommand(this.instance));
    }

    @Override
    public void onDisable() {

    }
}
